export default {
    home: {
        name: 'Home', path: '/',
    },
    stock: {
        name: 'Stocks', path: '/stock',
    },
    portfolio: {
        name: 'Portfolio', path: '/portfolio',
    }
}  