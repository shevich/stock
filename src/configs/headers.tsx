import React from 'react'
import ShowChartIcon from '@material-ui/icons/ShowChart';
import Portrait from '@material-ui/icons/Portrait'
import HomeIcon from '@material-ui/icons/Home';

import routes from 'constants/routes';

export const headers = [
    { ...routes.home, id: 0, icon: <HomeIcon /> },
    { ...routes.stock, id: 1, icon: <ShowChartIcon /> },
    { ...routes.portfolio, id: 2, icon: <Portrait /> },
]