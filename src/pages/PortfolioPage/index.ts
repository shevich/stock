import { connect } from 'react-redux'
import { Portfolio } from './PortfolioPage';
import { sell, sellAll } from 'store/modules/assets/actions';
import { IRootState } from 'store';

const mapStateToProps = ({ assets: { boughtAssets } }: IRootState) => ({
    assets: boughtAssets,
})

const mapDispatchToProps = {
    onSell: sell,
    onSellAll: sellAll,
}

export default connect(mapStateToProps, mapDispatchToProps)(Portfolio as any);