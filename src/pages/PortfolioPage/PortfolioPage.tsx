import React from 'react';
import PropTypes from 'prop-types';

import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import { Container } from '@material-ui/core';

import SellAsset from 'components/SellAsset';
import NoData from 'core/components/NoData';

import classes from './styles.module.scss'

export const Portfolio = ({ assets, onSell, onSellAll }: { assets: Array<any>, onSell: any, onSellAll: any}) => {
    if(!assets.length) {
        return <NoData message="You have bougth no assets"/>
    }

    return (
        <Container className={ classes.Portfolio }>
            <Button 
                className={ classes['Portfolio__sell-all'] } 
                variant="contained" 
                color="secondary"
                onClick={ onSellAll }>
                    Sell all
            </Button>
            <Box display="flex" justifyContent="space-between" flexWrap="wrap">
                { assets.map(asset => 
                    <SellAsset 
                        key={ asset.id } 
                        id={ asset.id }
                        name={ asset.name } 
                        price={ asset.price }
                        quantity={ asset.quantity }
                        onSell={ onSell }
                    />
                )}
            </Box>
        </Container>
    )
}

Portfolio.propTypes = {
    assets: PropTypes.array,
    onSell: PropTypes.func,
}

Portfolio.defaultProps = {
    onSell: () => {},
    assets: [],
}