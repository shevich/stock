import { connect } from 'react-redux'
import { StockPage } from './StocksPage';
import { buy } from 'store/modules/assets/actions';
import { IRootState } from 'store';

const mapStateToProps = ({ assets: { assets }}: IRootState) => ({
    assets
})

const mapDispatchToProps = {
    onBuy: buy
};

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(StockPage);
