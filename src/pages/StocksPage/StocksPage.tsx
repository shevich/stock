import React from 'react';
import PropTypes from 'prop-types';

import { Box } from '@material-ui/core';

import BuyAsset from 'components/BuyAsset';
import NoData from 'core/components/NoData';

export const StockPage = ({ assets, onBuy }: { assets: Array<any>, onBuy: any }) => {
    if(!assets.length) {
        return <NoData message="Sorry, there is no available assets to buy"/>
    }

    return (
        <>
            <Box display="flex" justifyContent="space-between" flexWrap="wrap">
                { assets.map(asset => 
                    <BuyAsset 
                        key={ asset.id } 
                        id={ asset.id }
                        name={ asset.name } 
                        price={ asset.price }
                        onBuy={ onBuy }
                    />
                )}
            </Box>
        </>
    )
}

StockPage.propTypes = {
    assets: PropTypes.array,
    onBuy: PropTypes.func,
}

StockPage.defaultProps = {
    onBuy: () => {},
    assets: [],
}