import { connect } from 'react-redux';
import { MainPage } from './MainPage';
import { initStock } from 'store/modules/assets/actions';
import { IRootState } from 'store';

// duplicated code automate it
const mapStateToProps = ({ assets: { money }}: IRootState) => ({
    money,
})

const mapDispatchToProps = {
    onStockInit: initStock,
}

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);