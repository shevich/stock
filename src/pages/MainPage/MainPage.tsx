import React, { useEffect } from 'react';
import PropTypes from 'prop-types'
import { Card, CardContent, Typography, Divider } from '@material-ui/core';
import styled from 'styled-components';

import { getCryptoCurrencyInfo } from 'services/api/coinmarket/coinmarket.service';

const StyledDivider = styled(Divider)`margin: 20px 0 !important`;

export const MainPage = ({ money, onStockInit }: { money: number, onStockInit: any }) => {
    useEffect(() => {
        const initStock = async () => {
            const { data } = await getCryptoCurrencyInfo();
            onStockInit(data);
        };
        initStock();
    }, [onStockInit])
    return (
        <Card>
            <CardContent>
                <Typography variant="h4">
                    Trade or View your portfolio
                </Typography>
                <StyledDivider />
                <Typography variant="subtitle1">
                  Your Funds: ${money.toFixed(2)}
                </Typography>
            </CardContent>
        </Card>
    )
}

MainPage.propTypes = {
    money: PropTypes.number.isRequired,
    onStockInit: PropTypes.func.isRequired,
}

MainPage.defaultProps = {
    money: 0,
}
