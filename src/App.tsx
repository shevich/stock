import React from 'react';
import { PersistGate } from 'redux-persist/integration/react'
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from "react-router-dom";

import Header from 'core/components/Header';
import ContentLayout from 'core/components/ContentLayout';
import Footer from 'core/components/Footer';
import Loader from 'core/components/Loader';

import getStore from 'store';

const { store, persistor } = getStore();

export default () => (
    <Provider store={ store }>
      <PersistGate loading={ <Loader /> } persistor={ persistor }>
        <Router>
          <Header />  
          <ContentLayout />
          <Footer />
        </Router> 
      </PersistGate>
    </Provider>
  );
