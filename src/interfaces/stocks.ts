export interface IAsset {
    id: number,
    name: string,
    price: number,
}

export interface IBougthAsset extends IAsset {
    quantity: number,
}

export enum AssetAction {
    buy = 'buy',
    sell = 'sell',
    sellAll = 'sell_all',
    initStock = 'init_stock',
}
