
import { IAsset, IBougthAsset } from "interfaces/stocks";

export interface IAssetState {
    money: number,
    assets: Array<IAsset>,
    boughtAssets: Array<IBougthAsset>,
  }
  
const getInitState = (): IAssetState => ({
    assets: [],
    boughtAssets: [],
    money: 10000,
})
 
export default getInitState();