import { AssetAction } from "interfaces/stocks";
import initState, { IAssetState } from './state';
import { sellAll, sell, buy } from 'services/asset.service';

const reducerName = 'assets';
const createActionName = (name: string) => `${reducerName}/${name}`;

export const actionTypes = {
  buy: createActionName(AssetAction.buy),
  sell: createActionName(AssetAction.sell),
  sellAll: createActionName(AssetAction.sellAll),
  initStock: createActionName(AssetAction.initStock),
}

// Possibly create Adapter to different API soursec
const initStock = (state: IAssetState, assets: Array<any>): IAssetState => {
  return {
    ...state,
    assets: assets.map(asset => ({
      name: asset.name,
      id: asset.id,
      price: Number(asset.quote.USD.price.toFixed(4)),
    }))
  }
}

interface ReducerAction {
  type: AssetAction,
  payload: any,
}

export default (state = initState, { type, payload }: ReducerAction) => {
  switch(type) {
      case actionTypes.buy: return buy(state, payload.assetId, payload.quantity);
      case actionTypes.sell: return sell(state, payload.assetId, payload.quantity);
      case actionTypes.sellAll: return sellAll(state);
      case actionTypes.initStock: return initStock(state, payload);
      default: return state;
  };
}