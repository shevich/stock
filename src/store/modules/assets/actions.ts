import { actionTypes } from './reducer';

export const buy = (assetId: number, quantity: number) => 
    ({ type: actionTypes.buy, payload: { assetId, quantity } });

export const sell = (assetId: number, quantity: number) => 
    ({ type: actionTypes.sell, payload: { assetId, quantity } });

export const sellAll = () => ({ type: actionTypes.sellAll })

export const initStock = (assets: Array<any>) => ({ type: actionTypes.initStock, payload: assets })
