export interface IUserState {
    name: string,
    password: string,
    email: string,
    isVerified: boolean,
}

export const initState: IUserState = {
    name: '',
    password: '',
    email: '',
    isVerified: false,
}

export default (state = initState) => state;