import { createStore, combineReducers } from 'redux'
import assetReducer from './modules/assets/reducer';
import { IAssetState } from './modules/assets/state';
import userReducer, { IUserState } from './modules/user/userReducer';

import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

const persistConfig = {
    key: 'root',
    storage,
}

const reducer = combineReducers({ assets: assetReducer, user: userReducer });
const persistedReducer = persistReducer(persistConfig, reducer);
const store = createStore(persistedReducer);
const persistor = persistStore(store);

// On each store update execute this function
const onStoreChange = () => console.log('Redux store state:', store.getState());

store.subscribe(onStoreChange)

export type IRootState = {
    assets: IAssetState,
    user: IUserState,
}

export default () => ({ store, persistor });