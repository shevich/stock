import React from 'react';
import PropTypes from 'prop-types';

import AppBar from '@material-ui/core/AppBar';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import { useHistory, useLocation } from "react-router-dom";

import { headers } from 'configs/headers';
import classes from './styles.module.scss';

export const Header = ({ money }: { money: number }) => {
    const history = useHistory();
    const location = useLocation();
    const initRoute = headers.findIndex(header => header.path === location.pathname);
    const [value, setValue] = React.useState(initRoute);
    const onTabChange = (event: Event, value: number) => {
        history.push(headers[value].path)
        setValue(value);
    }

    return (
        <AppBar position="static" color="primary" className={ classes.Header }>
            <Box display="flex" justifyContent="space-between">
                <Tabs value={value} onChange={onTabChange as any}>
                    { headers.map(item => 
                        (
                            <Tab 
                                key= { item.id }
                                className={classes['Header__tab']} 
                                label={item.name} 
                                icon={item.icon} />
           
                        ))}
                </Tabs>    
                <Typography className={ classes['Header__funds'] } variant="h5">
                    Funds: ${ money.toFixed(2) }
                </Typography> 
            </Box>          
        </AppBar>)
}

Header.propTypes = {
    money: PropTypes.number.isRequired,
}

Header.defaultProps = {
    money: 0,
}