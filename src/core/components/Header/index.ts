import { connect } from 'react-redux';
import { Header } from './Header';
import { IRootState } from 'store';

const mapStateToProps = ({ assets: { money } }: IRootState) => ({
    money
});

const emptyFunc = () => ({});

// @ts-ignore
export default connect(mapStateToProps, emptyFunc)(Header);