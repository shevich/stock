import React from 'react';
import Paper from '@material-ui/core/Paper';

import classes from './styles.module.scss';

export const Footer = () => (
    <Paper square variant='outlined' className={ classes.Footer }>
        © 2020 Sheva.com. All rights reserved        
    </Paper>
)
