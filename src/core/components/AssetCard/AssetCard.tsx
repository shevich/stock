import React, { useState } from 'react';
import PropTypes from 'prop-types';

import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import { Typography } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box';
import { Button } from '@material-ui/core';

import { validateAssetQuantity } from 'core/utils/validator';

import classes from './styles.module.scss';

export const AssetCard = (props: any) => {
    const { theme } = props;
    const [quantity, setQuantity] = useState(0);
    const [quantityErrors, setQuantityErrors] = useState('');

    const onQuantityChange = ({ target: { value }}: any) => {
        setQuantity(value);
        setQuantityErrors(validateAssetQuantity(value));
    }

    const isQuantityFilled = Boolean(quantity);
    const isValid = !quantityErrors && isQuantityFilled;
    const titleSlot = (
        <>
            <Typography className={classes['Asset__title']} variant='subtitle1'>{ props.title }</Typography>
            { props.subtitle }
        </>)
    return (
        <Card className={classes.Asset}>
            <CardHeader 
                className={ [classes['Asset__header'], theme['Asset__header']].join(' ') } 
                title={ titleSlot }
            />
            <CardContent>
                <Box display="flex" justifyContent="space-between">
                    <TextField 
                        placeholder="Quantity" 
                        variant="outlined" 
                        className={ classes['Asset__quantity-input'] }
                        margin="dense"
                        error={ Boolean(quantityErrors) }
                        helperText={ quantityErrors}
                        onChange={ onQuantityChange as any }
                    />
                    <Button 
                        variant="outlined" 
                        className={ [classes['Asset__action'], theme['Asset__action']].join(' ')  }
                        disabled={ !isValid }
                        onClick={ () => props.onClick(quantity) }
                    >
                            { props.actionText }
                    </Button>
                </Box>
            </CardContent>
        </Card>)
}

AssetCard.propTypes = {
    title: PropTypes.string.isRequired,
    subtitle: PropTypes.element,
    actionText: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    theme: PropTypes.object,
}

AssetCard.defaultProps = {
    subtitle: null,
    theme: {},
}