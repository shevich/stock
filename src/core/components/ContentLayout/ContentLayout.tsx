import React, { Suspense, lazy } from 'react';
import { Switch, Route } from "react-router-dom";
import Container from '@material-ui/core/Container';

import MainPage from 'pages/MainPage';
import Loader from 'core/components/Loader';
import ErrorBoundary from 'core/utils/ErrorBoundary';
import routes from 'constants/routes';
import styled from 'styled-components';

const StockPage = lazy(() => import('pages/StocksPage'));
const PortfolioPage = React.lazy(() => import('pages/PortfolioPage'));

const StyledContainer = styled(Container)`text-align: center`;

export const ContentLayout = () => (
  <ErrorBoundary>
    <StyledContainer>
      <Switch>
        <Suspense fallback={ <Loader /> }>
          <Route exact path={ routes.home.path } component={ MainPage } />
          <Route exact path={ routes.stock.path } component={ StockPage } />
          <Route exact path={ routes.portfolio.path } component={ PortfolioPage } />
        </Suspense>  
      </Switch>
    </StyledContainer>
  </ErrorBoundary>
);