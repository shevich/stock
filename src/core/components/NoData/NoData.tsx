import React from 'react';
import PropTypes from 'prop-types';
import Alert from '@material-ui/lab/Alert';

export const NoData = ({ message }: { message: string}) => (
    <Alert color='info' severity="info" variant='outlined'>
        { message }
    </Alert>)

NoData.propTypes = {
    message: PropTypes.string,
}

NoData.defaultProps = {
    message: 'Sorry, there is no data',
}