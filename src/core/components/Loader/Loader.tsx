import React from 'react';
import PropTypes from 'prop-types';
import CircularProgress from '@material-ui/core/CircularProgress';
import styled from 'styled-components';

const StyledCircularProgress = styled(CircularProgress)`
    margin: auto;
    text-align: center;
`

export const Loader = ({ size }: { size: string | number}) => 
    (<StyledCircularProgress size={ size } disableShrink />)

Loader.propTypes = {
    size: PropTypes.oneOfType([ PropTypes.string, PropTypes.number]),
}

Loader.defaultProps = {
    size: 60,
}