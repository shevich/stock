import React, { Component } from 'react';
import Alert from '@material-ui/lab/Alert';
import AlertTitle from '@material-ui/lab/AlertTitle';
import { isEmpty, get } from 'lodash';

export default class ErrorBoundary extends Component {
    state = {
        error: '',
        errorInfo: {}
    }
  
    componentDidCatch(error: any, errorInfo: any) {
      this.setState({ error, errorInfo });
    }
  
    render() {
        if (isEmpty(this.state.errorInfo)) {
            return this.props.children;
        } 

        return (
            <Alert color='error' severity='error'>
                <AlertTitle>{ this.state.error.toString() }</AlertTitle>
                <details style={{ whiteSpace: 'pre-wrap' }}>
                    { get(this.state.errorInfo, 'componentStack', 'No stack') }
                </details>
            </Alert>
        )
    }
  }