
import { isNaN } from 'lodash';

const emptyValue = '';
const noMessages = '';

enum ErrorType {
    LessThanZero = 'LessThanZero',
    IsNotNumber = 'IsNotNumber',
};

const errorMessage: Record<ErrorType, string> = {
    [ErrorType.IsNotNumber]: 'Please input a number',
    [ErrorType.LessThanZero]: 'Please input a valid positive number',
};

const errorRules: Record<ErrorType, Function> = {
    [ErrorType.IsNotNumber]: (value: any) => isNaN(value),
    [ErrorType.LessThanZero]: (value: any) => value <= 0
};

export const validateAssetQuantity = (value: string | number): string => {
    if(value === emptyValue) {
        return noMessages;
    }

    const quantity = Number(value);
    const errors = Object.keys(errorRules);

    const messages = errors.reduce((acc, error) =>
         errorRules[error as ErrorType](quantity) ? [...acc, errorMessage[error as ErrorType]]: acc
    ,[] as Array<string>);

    switch(messages.length) {
        case 0: return noMessages;
        case 1: return messages[0];
        default: return messages.map((message, index) => `${index + 1}) ${message}`).join('\n');
    }
};