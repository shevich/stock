import React from 'react';
import PropTypes from 'prop-types';

import { Typography } from '@material-ui/core';

import AssetCard from 'core/components/AssetCard';

import theme from './styles.module.scss';

export const BuyAsset = ({ id, name, price, onBuy }: any) => {
    const onBuyHandler = (quantity: string) =>
        onBuy(id, Number(quantity));

    const subTitleSlot = (
        <Typography variant='caption'>
            (Price: { price })
        </Typography>)
    return (
        <>
            <AssetCard 
                actionText="Buy"
                theme={ theme } 
                title={ name }
                subtitle={ subTitleSlot} 
                onClick={ onBuyHandler }
            />
        </>
    )
}

BuyAsset.propTypes = {
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    onBuy: PropTypes.func.isRequired,
}