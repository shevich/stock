import React from 'react';
import PropTypes from 'prop-types';

import { Typography } from '@material-ui/core';
import AssetCard from 'core/components/AssetCard';

import theme from './styles.module.scss';

export const SellAsset = ({ id, name, price, quantity, onSell }: any) => {
    const onSellHandler = (amount: string) => {
        onSell(id, Number(amount))
    }

    const subTitleSlot = (
        <Typography variant='caption'>
            (Price: { price } | Quantity: { quantity })
        </Typography>)
    return (
        <>
            <AssetCard 
                actionText="Sell"
                theme={ theme } 
                title={ name }
                subtitle={ subTitleSlot } 
                onClick={ onSellHandler }
            />
        </>
    )
}

SellAsset.propTypes = {
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    quantity: PropTypes.number.isRequired,
    onSell: PropTypes.func.isRequired,
}