
import { AxiosResponse } from 'axios';
import API from "./index";
import { endpoints } from '../../endpoints';
import { headers } from './config'; 
import { IAsset } from "interfaces/stocks";

const onSuccess = ({ data }: AxiosResponse) => data;

export const getCryptoCurrencyInfo = (
    start = 1,
    limit = 30,
    convert = "USD"
  ): Promise<{ data: IAsset[]}> =>
    API.get(endpoints.cryptocurrency(), {
        ...headers,
        params: {
          start,
          limit,
          convert
        }
      }).then(onSuccess);
  