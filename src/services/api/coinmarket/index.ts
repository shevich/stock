import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';
import { endpoints } from 'services/endpoints';
import { IAPI, ResponseData } from 'interfaces/api';
import { headers } from './config';

const requestConfig: AxiosRequestConfig = {
  headers,
  baseURL: endpoints.coinmarketcap,
  timeout: 2500,
}

const handleSuccessResponse = (response: AxiosResponse) => response;

const handleFailedResponse = (response: any) => {
  console.log('Response failed: ', response);
  return response;
}

class API implements IAPI {
  private instance: AxiosInstance;

  constructor() {
    this.instance = axios.create(requestConfig);
    this.instance.interceptors.response
      .use(handleSuccessResponse, handleFailedResponse);
  }

  get<R=AxiosResponse<ResponseData>>(url: string, config?: AxiosRequestConfig): Promise<R> {
    return this.instance.get(url, config)
  }
  post<R=AxiosResponse<ResponseData>>(url: string, data?: any, config?: AxiosRequestConfig): Promise<R> {
    return this.instance.post(url, data, config)
  }
  delete<R=AxiosResponse<ResponseData>>(url: string, config?: AxiosRequestConfig): Promise<R> {
    return this.instance.delete(url, config)
  }
  put<R=AxiosResponse<ResponseData>>(url: string, data?: any, config?: AxiosRequestConfig): Promise<R> {
    return this.instance.put(url, data, config)
  }
  patch<R=AxiosResponse<ResponseData>>(url: string, data?: any, config?: AxiosRequestConfig): Promise<R> {
    return this.instance.patch(url, data, config)
  }
}

export default new API();