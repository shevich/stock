import { find, clone, filter } from 'lodash';
import { IAsset, IBougthAsset } from "interfaces/stocks";
import { IAssetState } from 'store/modules/assets/state';

// TODO: Move to server logic

const getAsset = (assets: Array<IAsset | IBougthAsset>, assetId: number) =>
    clone(find(assets, ['id', assetId]));

const calculateCost = (assets: Array<IBougthAsset>) =>
    assets.reduce((result, asset) => 
        result + asset.price * asset.quantity, 0);


const canBuy = (money: number, price: number, quantity: number) => {
    const requiredMoney = price * quantity;
    return money > requiredMoney;
}

export const sellAll = ({ assets, boughtAssets, money}: IAssetState) => ({
    money: calculateCost(boughtAssets) + money,
    assets: assets,
    boughtAssets: [],
});

export const sell = (state: IAssetState, assetId: number, quantity: number) => {
    const asset = getAsset(state.boughtAssets, assetId) as IBougthAsset;
    if(!asset) {
        return state;
    };
    const canSell = asset.quantity >= quantity;
    if(!canSell) {
        return state;
    }

    const isAssetSold = asset.quantity === quantity; 
    const updatedMoney = state.money + quantity * asset.price;
    const updatedAsset = { ...asset, quantity: asset.quantity - quantity };
    const restAssets = filter(state.boughtAssets, asset => asset.id !== assetId);
    const boughtAssets = isAssetSold ? restAssets: [...restAssets, updatedAsset];
    
    return ({
      ...state,
      boughtAssets,
      money: updatedMoney,
    })
}

export const buy = (state: IAssetState, assetId: number, quantity: number) => {
    const asset = getAsset(state.assets, assetId) as IAsset;
  
    if(!asset || !canBuy(state.money, asset.price, quantity)) {
      return state;
    }
  
    const updatedMoney = state.money - quantity * asset.price;
    const restBoughtAssets = filter(state.boughtAssets, asset => asset.id !== assetId);
    const boughtAsset = getAsset(state.boughtAssets, assetId) as IBougthAsset;
    const boughtAssets = boughtAsset ? 
        [...restBoughtAssets, { ...boughtAsset, quantity: boughtAsset.quantity + quantity }]:
        [...restBoughtAssets, { ...asset, quantity }];
 
    return ({
        ...state,
        boughtAssets,
        money: updatedMoney,
    })
  }
