# Changelog

All notable changes to this project will be documented in this file.

## [1.0.1] - 2020-04-28

### Changed

#### Functionality

- Add Typescript
- Add interfaces folder
- Add sell-all button on Portfolio page
- Change web-site title and favicon
- Install axios
- Add wrapper for axios and configs for coinmarket API
- Create ContentLayout
- Install node-sass, styled-components
- Add validator for asset card
- Install persist-store
- Refactor store

#### Improvements

- Move common functionality of Buy-and-SellAssets to Assets
- Refactor styles to scss-modules and styled-components
- Remove yarn.lock
- Refactor asset-module store

#### Bugs

- Fix initing header tabs

#### Docs

- ChangeLog.md

### Fixed

- Header init tab value

## [1.0.0] - 2020-04-24

### Changed

- Init project
- Add header
- Add footer
- Add Main page
- Add Stock page
- Add Portfolio page
